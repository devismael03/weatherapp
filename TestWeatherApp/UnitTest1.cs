using NUnit.Framework;
using System.Collections.Generic;
using WeatherApp;
using WeatherApp.Controllers;

namespace TestWeatherApp
{
    public class Tests
    {
        private WeatherForecastService _service;
        [SetUp]
        public void Setup()
        {
            _service = new WeatherForecastService();
        }

        [Test]
        public void Test1()
        {
            List<WeatherForecast> weatherItems = _service.GetWeatherForecast();
            Assert.IsNotNull(weatherItems);

            Assert.AreEqual(5, weatherItems.Count);
        }
    }
}